/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { Sections } from "./Sections";

/**
 * Provides utility for splitting a .pbc file into its sections. The documentation for the .pbc
 * format can be found <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-contract-file">here</a>.
 *
 * <p>A .pbc file is a single file that contains all the necessary elements needed to deploy a smart
 * contract to the Partisia Blockchain.
 *
 * <p>A .pbc file contains an ABI section, which is a binary interface containing the signatures of
 * the available actions and named types of a contract, and a WASM section, which is the compiled,
 * executable WebAssembly code of a contract.
 *
 * <p>A .pbc file of a ZK-contract will also contain a ZKBC section, which is a bytecode format that
 * contains the circuit to be executed in a ZK-computation.
 */
export class PbcFile {
  /** The header bytes of the .pbc format. */
  public static readonly PBC_HEADER_BYTES: Buffer = new Buffer([0x50, 0x42, 0x53, 0x43]);

  /** Identifier byte indicating the beginning of an ABI section of a pbc file. */
  public static readonly ABI_IDENTIFIER_BYTE: number = 0x1;

  /** Identifier byte indicating the beginning of an WASM section of a pbc file. */
  public static readonly WASM_IDENTIFIER_BYTE: number = 0x2;

  /** Identifier byte indicating the beginning of an ZKBC section of a pbc file. */
  public static readonly ZK_IDENTIFIER_BYTE: number = 0x3;

  /** Whether this is the .pbc file of a ZK-contract. */
  public readonly isZk: boolean;

  /**
   * Identifiers given in the order corresponding to the <a
   * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format">".pbc
   * format"</a>.
   */
  private static readonly IDENTIFIERS: Buffer = Buffer.from([
    PbcFile.ABI_IDENTIFIER_BYTE,
    PbcFile.WASM_IDENTIFIER_BYTE,
    PbcFile.ZK_IDENTIFIER_BYTE,
  ]);
  private readonly abiBytes: Buffer;
  private readonly wasmBytes: Buffer;
  private readonly zkbcBytes: Buffer | undefined;

  private constructor(
    abiBytes: Buffer,
    wasmBytes: Buffer,
    zkbcBytes: Buffer | undefined,
    isZk: boolean
  ) {
    this.abiBytes = abiBytes;
    this.wasmBytes = wasmBytes;
    this.zkbcBytes = zkbcBytes;
    this.isZk = isZk;
  }

  /**
   * Create a PbcFile from a byte array following the PBC <a
   * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-contract-file">format</a>.
   *
   * @param pbcBytes built contract bytes in .pbc format.
   * @return the .pbc file split into it's sections
   * @throws Error if the given bytes don't contain the PBSC header string, an ABI
   *     section or a WASM section or if the sectioned part of the bytes is ill-formed.
   */
  public static fromBytes(pbcBytes: Buffer): PbcFile {
    const inputStream: BigEndianByteInput = new BigEndianByteInput(pbcBytes);
    const inputHeaderBytes = inputStream.readBytes(4);

    if (!inputHeaderBytes.equals(PbcFile.PBC_HEADER_BYTES)) {
      throw Error("Given bytes do not contain PBC header!");
    }

    let sections: Sections;
    try {
      sections = Sections.fromBytes(inputStream, PbcFile.IDENTIFIERS);
    } catch (e) {
      throw Error("Exception encountered while parsing sectioned bytes:\n" + e);
    }

    return this.fromSections(sections);
  }

  private static fromSections(sections: Sections): PbcFile {
    if (!sections.hasSection(this.ABI_IDENTIFIER_BYTE)) {
      throw Error("Given bytes contain no ABI section!");
    }
    if (!sections.hasSection(this.WASM_IDENTIFIER_BYTE)) {
      throw Error("Given bytes contain no WASM section!");
    }

    const abiBytes: Buffer = sections.getSection(this.ABI_IDENTIFIER_BYTE)!;
    const wasmBytes: Buffer = sections.getSection(this.WASM_IDENTIFIER_BYTE)!;
    let zkbcBytes: Buffer | undefined = undefined;
    let isZk: boolean = false;
    if (sections.hasSection(this.ZK_IDENTIFIER_BYTE)) {
      zkbcBytes = sections.getSection(this.ZK_IDENTIFIER_BYTE);
      isZk = true;
    }

    return new PbcFile(abiBytes, wasmBytes, zkbcBytes, isZk);
  }

  /**
   * Build ZKWA formatted bytes, following the definition of the ZKWA format <a
   * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#zkwa-format">here</a>.
   * Both a WASM and ZKBC section must be present, to create ZKWA bytes.
   *
   * <p>ZKWA is the format used to deploy ZK-contracts to the Partisia Blockchain.
   *
   * @return the ZKWA formatted bytes.
   * @throws Error if no ZKBC section is present.
   */
  public getZkwaBytes(): Buffer {
    if (!this.isZk) {
      throw Error(
        "Could not construct ZKWA file, the ZKBC section was missing in the given bytes."
      );
    }
    return BigEndianByteOutput.serialize((bigEndianByteOutput) => {
      bigEndianByteOutput.writeU8(PbcFile.WASM_IDENTIFIER_BYTE);
      bigEndianByteOutput.writeU32(this.wasmBytes.length);
      bigEndianByteOutput.writeBytes(this.wasmBytes);
      bigEndianByteOutput.writeU8(PbcFile.ZK_IDENTIFIER_BYTE);
      bigEndianByteOutput.writeU32(this.zkbcBytes!.length);
      bigEndianByteOutput.writeBytes(this.zkbcBytes!);
    });
  }

  /**
   * Returns the content bytes of the ABI section.
   *
   * @return the ABI content bytes.
   */
  public getAbiBytes(): Buffer {
    return this.abiBytes;
  }

  /**
   * Returns the content bytes of the WASM section.
   *
   * @return the WASM content bytes.
   */
  public getWasmBytes(): Buffer {
    return this.wasmBytes;
  }

  /**
   * Returns the content bytes of the ZKBC section.
   *
   * @return the ZKBC content bytes.
   */
  public getZkbcBytes(): Buffer {
    if (!this.isZk) {
      throw Error("Cannot get ZKBC bytes from a public contract");
    }
    return this.zkbcBytes!;
  }
}
