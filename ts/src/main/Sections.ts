/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput, BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";

/**
 * Sections of a sectioned file. A section follows the following <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#sections">format</a>.
 *
 * <p>A section consists of an identifier byte, a 32-bit unsigned integer indicating the length of
 * the section's content and then the section's content bytes.
 *
 * <p>Can be used to build sectioned files, eg. a <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format">.pbc
 * file</a>.
 */
export class Sections {
  /** Map from section identifiers to content bytes. */
  private readonly sections: Map<number, Buffer>;

  /** Ordered set of allowed identifiers for the given sections. */
  public readonly allowedIdentifiers: Buffer;

  /**
   * Creates an empty collection of sections.
   *
   * @param allowedIdentifiers byte array containing the allowed identifiers for the given sections.
   */
  public constructor(allowedIdentifiers: Buffer) {
    this.sections = new Map();
    this.allowedIdentifiers = allowedIdentifiers;
  }

  /**
   * Returns the bytes of the section corresponding to the given identifier.
   *
   * @param identifier identifier of the section to be returned
   * @return the section content bytes
   */
  public getSection(identifier: number): Buffer | undefined {
    return this.sections.get(identifier);
  }

  /**
   * Serializes the added sections into a byte array. The sections are ordered by their identifiers
   * from lowest to highest.
   *
   * @return the serialized sections.
   */
  public getSerializedBytes(): Buffer {
    return BigEndianByteOutput.serialize((bigEndianByteOutput) => {
      for (const identifier of this.allowedIdentifiers) {
        if (this.sections.has(identifier)) {
          bigEndianByteOutput.writeU8(identifier);
          bigEndianByteOutput.writeU32(this.sections.get(identifier)!.length);
          bigEndianByteOutput.writeBytes(this.sections.get(identifier)!);
        }
      }
    });
  }

  /**
   * Adds a section of bytes with a given header byte to the collection of sections. If a section
   * with the provided identifier has already been added then the content of that section is
   * overwritten.
   *
   * @param identifier identifier byte indicating the type of the section to be added.
   * @param sectionBytes the binary data of the section to be added.
   * @throws Error if the given identifier is not in the set of allowed identifiers.
   */
  public addSection(identifier: number, sectionBytes: Buffer) {
    if (this.allowedIdentifiers.indexOf(identifier) === -1) {
      throw Error(`Section identifier ${identifier} is not allowed.`);
    }
    this.sections.set(identifier, sectionBytes);
  }

  /**
   * Check if the sections contains a section with the given identifier.
   *
   * @param identifier identifier byte indicating the type of the section.
   * @return whether a section with the given identifier is contained.
   */
  public hasSection(identifier: number): boolean {
    return this.sections.has(identifier);
  }

  /**
   * Creates Sections by parsing sectioned binary data. Sections must come in ascending order by id.
   *
   * @param byteInput Stream of bytes to parse.
   * @param allowedIdentifiers List of allowed section identifiers
   * @return the parsed sections.
   * @exception Error If the input bytes are encoded incorrectly.
   */
  public static fromBytes(byteInput: BigEndianByteInput, allowedIdentifiers: Buffer): Sections {
    const sections = new Sections(allowedIdentifiers);
    let previousSectionId: number = 0;
    let sectionId: number | null = this.readNextSectionId(byteInput);

    while (sectionId !== null) {
      const maxIdReached = previousSectionId == 255;
      if (maxIdReached) {
        throw Error(
          `Invalid section ${sectionId}: Sections past a section with the id 255 are not allowed.`
        );
      }

      const duplicateSectionId = sections.hasSection(sectionId);
      if (duplicateSectionId) {
        throw Error(`Invalid section ${sectionId}: Duplicated section id`);
      }

      const incorrectlyOrdered = sectionId < previousSectionId;
      if (incorrectlyOrdered) {
        throw Error(
          `Invalid section ${sectionId}: incorrectly ordered. Expected section` +
            ` with id of at least ${previousSectionId + 1}`
        );
      }

      const sectionContent: Buffer = this.readSectionContent(byteInput);
      sections.addSection(sectionId, sectionContent);

      previousSectionId = sectionId;
      sectionId = this.readNextSectionId(byteInput);
    }
    return sections;
  }

  private static readNextSectionId(byteInput: BigEndianByteInput): number | null {
    try {
      return byteInput.readU8();
    } catch {
      return null;
    }
  }

  private static readSectionContent(byteInput: BigEndianByteInput): Buffer {
    // Read length of the section's content
    const sectionLen: number = byteInput.readU32();

    // Try to read and return the indicated number of bytes
    return byteInput.readBytes(sectionLen);
  }
}
