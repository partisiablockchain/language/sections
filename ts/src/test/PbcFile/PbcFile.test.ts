/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { PbcFile } from "../../main";
import { expect, test } from "@jest/globals";
import * as fs from "fs";
import path from "path";

/* Parsing .pbc files from bytes */

/** PbcFile can be created from bytes. */
test("createPbcFileFromBytes", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.pbc")
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  const expectedAbi = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.abi")
  );
  const expectedZkwa = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.zkwa")
  );

  expect(pbcFile.getAbiBytes()).toStrictEqual(expectedAbi);
  expect(pbcFile.getZkwaBytes()).toStrictEqual(expectedZkwa);
});

/** A byte representation of a PBC file, must contain the PBC header, "PBSC". */
test("createFromBytesWithoutPbcHeader", () => {
  const pbcBytes: Buffer = Buffer.from(
    "5042" + // PB
      "0100000000" + // Section 1: 0 bytes
      "0200000005" + // Section 2: 5 bytes
      "5051525354", // Section 2 data
    "hex"
  );

  expect(() => PbcFile.fromBytes(pbcBytes)).toThrowError("Given bytes do not contain PBC header!");
});

/** The byte representation of a PBC file must contain an ABI section. */
test("createFromBytesWithoutAbiSection", () => {
  const pbcBytes: Buffer = Buffer.from(
    "50425343" + // PBSC header bytes
      "0200000005" + // Section 2: 5 bytes
      "5051525354", // Section 2 data
    "hex"
  );

  expect(() => PbcFile.fromBytes(pbcBytes)).toThrowError("Given bytes contain no ABI section!");
});

/** The byte representation of a PBC file must contain a WASM section. */
test("createFromBytesWithoutWasmSection", () => {
  const pbcBytes: Buffer = Buffer.from(
    "50425343" + // PBSC header bytes
      "0100000005" + // Section 1: 5 bytes
      "5051525354", // Section 1 data
    "hex"
  );

  expect(() => PbcFile.fromBytes(pbcBytes)).toThrowError("Given bytes contain no WASM section!");
});

/** The sections of a PBC file must be correctly ordered. */
test("createFromBytesWithIncorrectlyOrderedSections", () => {
  const pbcBytes: Buffer = Buffer.from(
    "50425343" + // PBSC header bytes
      "0200000000" + // Section 2: 0 bytes
      "0100000005" + // Section 1: 5 bytes
      "5051525354", // Section 1 data
    "hex"
  );

  expect(() => PbcFile.fromBytes(pbcBytes)).toThrowError("Invalid section 1: incorrectly ordered.");
});

/** A PBC file must not contain duplicate sections. */
test("createFromBytesWithDuplicateSections", () => {
  const pbcBytes: Buffer = Buffer.from(
    "50425343" + // PBSC header bytes
      "0200000000" + // Section 2: 0 bytes
      "0200000005" + // Section 1: 5 bytes
      "5051525354", // Section 1 data
    "hex"
  );

  expect(() => PbcFile.fromBytes(pbcBytes)).toThrowError(
    "Exception encountered while parsing sectioned bytes:\n" +
      "Error: Invalid section 2: Duplicated section id"
  );
});

/* Querying sections of a parsed .pbc file */

/** WASM bytes can be extracted from a .pbc file. */
test("getWasmBytes", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.pbc")
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  const expectedWasm = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.wasm")
  );

  expect(pbcFile.getWasmBytes()).toStrictEqual(expectedWasm);
});

/** ZKBC bytes can be extracted from a .pbc file. */
test("getZkbcBytes", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.pbc")
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  const expectedZkbc = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.zkbc")
  );

  expect(pbcFile.getZkbcBytes()).toStrictEqual(expectedZkbc);
});

/** ZKWA bytes can be extracted from a .pbc file. */
test("getZkwaBytes", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.pbc")
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  const expectedZkwa = readBytesFromFile(
    path.join(__dirname, "../", "resources/references/zk_voting_simple/zk_voting_simple.zkwa")
  );

  expect(pbcFile.getZkwaBytes()).toStrictEqual(expectedZkwa);
});

/** ZKBC bytes cannot be extracted from the .pbc file of a public contract. */
test("getZkbcBytesPublicContract", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(
      __dirname,
      "../",
      "resources/references/multi_voting_contract/multi_voting_contract.pbc"
    )
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  expect(() => pbcFile.getZkbcBytes()).toThrowError("Cannot get ZKBC bytes from a public contract");
});

/** ZKWA bytes cannot be constructed from a public contract. */
test("getZkwaBytesPublicContract", () => {
  const zkVotingSimplePbcBytes = readBytesFromFile(
    path.join(
      __dirname,
      "../",
      "resources/references/multi_voting_contract/multi_voting_contract.pbc"
    )
  );
  const pbcFile: PbcFile = PbcFile.fromBytes(zkVotingSimplePbcBytes);

  expect(() => pbcFile.getZkwaBytes()).toThrowError(
    "Could not construct ZKWA file, the ZKBC section was missing in the given bytes."
  );
});

function readBytesFromFile(path: string): Buffer {
  try {
    return fs.readFileSync(path);
  } catch (e) {
    throw Error(`Error reading file: ${e}`);
  }
}
