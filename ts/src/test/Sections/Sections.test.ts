/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Sections } from "../../main";
import { expect, test } from "@jest/globals";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";

/* Parse bytes into sections */

/**
 * It's possible to parse well-formed, sectioned bytes into Sections provided a list of allowed
 * identifiers.
 */
test("parseSectionedBytes", () => {
  const data: Buffer = Buffer.from(
    "0100000005" + // Section 1: 5 bytes
      "5051525354" + // Section 1 data
      "0200000000", // Section 2: 0 bytes)
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = Sections.fromBytes(input, allowedIdentifiers);
  expect(() => sections.hasSection(0x01)).toBeTruthy();
  expect(() => sections.hasSection(0x02)).toBeTruthy();
  expect(sections.getSerializedBytes()).toStrictEqual(data);
});

/** Sections must be ordered by their identifiers, lowest to highest. */
test("parseIncorrectlyOrderedSections", () => {
  const data: Buffer = Buffer.from(
    "0200000000" + // Section 2: 0 bytes
      "0100000005" + // Section 1: 5 bytes
      "5051525354", // Section 1 data
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError(
    "Invalid section 1: incorrectly ordered. Expected section with id of at least 3"
  );
});

/** No sections are allowed after a section with max id 255. Ids are unsigned bytes. */
test("parseIncorrectlyOrderedSectionsMaxId", () => {
  const data: Buffer = Buffer.from(
    "FF00000005" + // Section 255: 5 bytes
      "5051525354" + // Section 255 data
      "0200000000", // Section 2: 0 bytes
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0xff, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError(
    "Invalid section 2: Sections past a section with the id 255 are not allowed."
  );
});

/** Indicating the length of a section to be negative is not allowed. */
test("parseSectionNegativeLength", () => {
  const data: Buffer = Buffer.from(
    "01FFFFFFFF" + // Section 1: -1 bytes
      "5051525354" + // Section 1 data
      "0200000000", // Section 2: 0 bytes
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError("Unable to read bytes");
});

/**
 * A section may not be indicated to be longer than the remainder of the proceeding bytes to be
 * parsed.
 */
test("parseSectionLengthTooLong", () => {
  const data: Buffer = Buffer.from(
    "010F000000" + // Section 2: 0 bytes
      "0200000005" + // Section 2: 5 bytes
      "5051525354", // Section 2 data
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError("Unable to read bytes");
});

/** Sectioned bytes may not contain duplicate identifiers. */
test("parseSectionsDuplicateIdentifier", () => {
  const data: Buffer = Buffer.from(
    "0200000000" + // Section 2: 0 bytes
      "0200000005" + // Section 2: 5 bytes
      "5051525354", // Section 2 data
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError(
    "Invalid section 2: Duplicated section id"
  );
});

/** Parsed Sections may not contain identifiers outside the list of allowed identifiers. */
test("parseSectionsNotAllowedIdentifier", () => {
  const data: Buffer = Buffer.from(
    "0200000000" + // Section 2: 0 bytes
      "0300000005" + // Section 3: 5 bytes
      "5051525354", // Section 3 data
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  expect(() => Sections.fromBytes(input, allowedIdentifiers)).toThrowError(
    "Section identifier 3 is not allowed."
  );
});

/** It's possible to add sections individually given an identifier byte and content bytes. */
test("addSection", () => {
  const sectionContent: Buffer = Buffer.from("5051525354", "hex");
  const allowedIdentifiers: Buffer = Buffer.from([0x01]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x1, sectionContent);
  expect(sections.hasSection(0x01)).toBeTruthy();
  expect(sections.getSection(0x01)?.length).toBe(sectionContent.length);
  expect(sections.getSection(0x01)!).toStrictEqual(sectionContent);
});

/**
 * Adding a section with an identifier that has already been added overwrites the content of that
 * section.
 */
test("overWriteExistingSection", () => {
  const sectionContent: Buffer = Buffer.from("5051525354", "hex");
  const allowedIdentifiers: Buffer = Buffer.from([0x01]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x1, sectionContent);
  expect(sections.getSection(0x01)).toStrictEqual(sectionContent);

  const sectionContentAltered: Buffer = Buffer.from("5051525355", "hex");
  sections.addSection(0x1, sectionContentAltered);
  expect(sections.getSection(0x01)).toStrictEqual(sectionContentAltered);
});

/** Section ids larger than 127 are treated as unsigned integers. */
test("addSectionWithIdLargerThan127", () => {
  const data: Buffer = Buffer.from(
    "0000000000" + // Section 0: 0 bytes
      "8000000000" + // Section 128: 0 bytes (should be treated as unsigned integer)
      "FF00000000", // Section 255: 0 bytes (should be treated as unsigned integer)
    "hex"
  );
  const allowedIdentifiers: Buffer = Buffer.from([0x00, 0x80, 0xff]);
  const inputStream = new BigEndianByteInput(data);
  const sections: Sections = Sections.fromBytes(inputStream, allowedIdentifiers);
  expect(sections.hasSection(0x00)).toBeTruthy();
  expect(sections.hasSection(0x80)).toBeTruthy();
  expect(sections.hasSection(0xff)).toBeTruthy();
});

/**
 * An added section must not have an identifier which is not given in the list of allowed
 * identifiers.
 */
test("addSectionWithIllegalIdentifier", () => {
  const sectionContent: Buffer = Buffer.from("0102030405", "hex");
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = new Sections(allowedIdentifiers);
  expect(() => sections.addSection(0x03, sectionContent)).toThrowError(
    "Section identifier 3 is not allowed."
  );
});

/* Storing sections and querying stored sections */

/** It is possible to retrieve the content of an added section by its identifier. */
test("getSection", () => {
  const sectionContent1: Buffer = Buffer.from("5051525354", "hex");
  const sectionContent2: Buffer = Buffer.from("5556575859", "hex");

  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x1, sectionContent1);
  sections.addSection(0x2, sectionContent2);

  expect(sections.getSection(0x01)).toStrictEqual(sectionContent1);
  expect(sections.getSection(0x02)).toStrictEqual(sectionContent2);
});

/** Querying the identifier of a section that has not been added returns null. */
test("getSectionNotPresent", () => {
  const sectionContent: Buffer = Buffer.from("5051525354", "hex");

  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x1, sectionContent);

  expect(sections.getSection(0x02)).toStrictEqual(undefined);
});

/* Serializing stored sections */

/**
 * It's possible to serialize added sections into well-formed, sectioned bytes.
 *
 * <p>Each section contains an identifier, a 32-bit, unsigned, big-endian integer indicating the
 * length of the section and finally the content bytes of the section.
 */
test("serializeAddedSections", () => {
  const sectionContent1: Buffer = Buffer.from("5051525354", "hex");
  const sectionContent2: Buffer = Buffer.from("5657585950", "hex");

  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x1, sectionContent1);
  sections.addSection(0x2, sectionContent2);

  const sectionBytesStream = new BigEndianByteInput(sections.getSerializedBytes());
  expect(sectionBytesStream.readI8()).toStrictEqual(0x01);
  expect(sectionBytesStream.readI32()).toStrictEqual(sectionContent1.length);
  expect(sectionBytesStream.readBytes(sectionContent1.length)).toStrictEqual(sectionContent1);
  expect(sectionBytesStream.readI8()).toStrictEqual(0x02);
  expect(sectionBytesStream.readI32()).toStrictEqual(sectionContent2.length);
  expect(sectionBytesStream.readBytes(sectionContent1.length)).toStrictEqual(sectionContent2);
});

/**
 * The serialized sections are ordered by identifier from smallest to largest even if they are
 * added in opposite order.
 */
test("addSectionsOppositeOrder", () => {
  const sectionContent1: Buffer = Buffer.from("5051525354", "hex");
  const sectionContent2: Buffer = Buffer.from("5657585950", "hex");

  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02]);
  const sections: Sections = new Sections(allowedIdentifiers);
  sections.addSection(0x2, sectionContent2);
  sections.addSection(0x1, sectionContent1);

  const sectionBytesStream = new BigEndianByteInput(sections.getSerializedBytes());
  expect(sectionBytesStream.readI8()).toStrictEqual(0x01);
  expect(sectionBytesStream.readI32()).toStrictEqual(sectionContent1.length);
  expect(sectionBytesStream.readBytes(sectionContent1.length)).toStrictEqual(sectionContent1);
  expect(sectionBytesStream.readI8()).toStrictEqual(0x02);
  expect(sectionBytesStream.readI32()).toStrictEqual(sectionContent2.length);
  expect(sectionBytesStream.readBytes(sectionContent1.length)).toStrictEqual(sectionContent2);
});

/**
 * Only the sections present in the parsed input bytes are present when the parsed sections are
 * serialized, despite the list of allowed identifiers containing more.
 */
test("getSerializedBytesManyAllowedIdentifiers", () => {
  const data: Buffer = Buffer.from(
    "0200000000" + // Section 2: 5 bytes
      "0300000005" + // Section 3: 5 bytes)
      "5051525354", // Section 3 data
    "hex"
  );
  const input: BigEndianByteInput = new BigEndianByteInput(data);
  const allowedIdentifiers: Buffer = Buffer.from([0x01, 0x02, 0x3, 0x4, 0x5]);
  const sections: Sections = Sections.fromBytes(input, allowedIdentifiers);
  expect(sections.getSerializedBytes()).toStrictEqual(data);
});
