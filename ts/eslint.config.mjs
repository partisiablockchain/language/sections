import _import from "eslint-plugin-import";
import typescriptEslint from "@typescript-eslint/eslint-plugin";
import { fixupPluginRules } from "@eslint/compat";
import globals from "globals";
import tsParser from "@typescript-eslint/parser";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [
    ...compat.extends("eslint:recommended", "plugin:@typescript-eslint/recommended"),
    {
        plugins: {
            import: fixupPluginRules(_import),
            "@typescript-eslint": typescriptEslint,
        },

        languageOptions: {
            globals: {
                ...globals.browser,
            },

            parser: tsParser,
            ecmaVersion: 12,
            sourceType: "module",

            parserOptions: {
                project: "./tsconfig.json",
            },
        },

        rules: {
            semi: ["error", "always"],

            quotes: ["error", "double", {
                avoidEscape: true,
            }],

            "no-console": ["warn", {
                allow: ["warn", "error"],
            }],

            "arrow-parens": ["error", "always"],
            "no-param-reassign": "error",
            "no-sequences": "error",
            "object-shorthand": "error",
            "prefer-const": "error",
            "prefer-object-spread": "error",
            "import/no-default-export": "error",

            "import/no-internal-modules": ["error", {
                allow: ["unfetch/polyfill/index", "promise-polyfill/dist/polyfill"],
            }],

            "import/no-extraneous-dependencies": "error",

            "@typescript-eslint/array-type": ["error", {
                default: "array-simple",
            }],

            "@typescript-eslint/prefer-readonly": "error",
            "@typescript-eslint/no-dynamic-delete": "error",
            "@typescript-eslint/no-require-imports": "error",

            "@typescript-eslint/strict-boolean-expressions": ["error", {
                allowString: false,
                allowNumber: false,
                allowNullableObject: false,
            }],
        },
    },
];