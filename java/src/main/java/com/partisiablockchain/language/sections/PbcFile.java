package com.partisiablockchain.language.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Provides utility for splitting a .pbc file into its sections. The documentation for the .pbc
 * format can be found <a href=
 * "https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-contract-file">here</a>.
 *
 * <p>A .pbc file is a single file that contains all the necessary elements needed to deploy a smart
 * contract to the Partisia Blockchain.
 *
 * <p>A .pbc file contains an ABI section, which is a binary interface containing the signatures of
 * the available actions and named types of a contract, and a WASM section, which is the compiled,
 * executable WebAssembly code of a contract.
 *
 * <p>A .pbc file of a ZK-contract will also contain a ZKBC section, which is a bytecode format that
 * contains the circuit to be executed in a ZK-computation.
 */
public final class PbcFile {
  /** The header bytes of the .pbc format. */
  private static final byte[] PBC_HEADER_BYTES = new byte[] {0x50, 0x42, 0x53, 0x43};

  /** Identifier byte indicating the beginning of an ABI section of a pbc file. */
  public static final byte ABI_IDENTIFIER_BYTE = 0x1;

  /** Identifier byte indicating the beginning of an WASM section of a pbc file. */
  public static final byte WASM_IDENTIFIER_BYTE = 0x2;

  /** Identifier byte indicating the beginning of an ZKBC section of a pbc file. */
  public static final byte ZK_IDENTIFIER_BYTE = 0x3;

  /** Whether this is the .pbc file of a ZK-contract. */
  public final boolean isZk;

  /**
   * Identifiers given in the order corresponding to the <a href=
   * "https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format">".pbc
   * format"</a>.
   */
  private static final List<Byte> IDENTIFIERS =
      List.of(ABI_IDENTIFIER_BYTE, WASM_IDENTIFIER_BYTE, ZK_IDENTIFIER_BYTE);

  private final byte[] abiBytes;
  private final byte[] wasmBytes;
  private final byte[] zkbcBytes;

  private PbcFile(byte[] abiBytes, byte[] wasmBytes, byte[] zkbcBytes, boolean isZk) {
    this.abiBytes = abiBytes;
    this.wasmBytes = wasmBytes;
    this.zkbcBytes = zkbcBytes;
    this.isZk = isZk;
  }

  /**
   * Create a PbcFile from a byte array following the PBC <a href=
   * "https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-contract-file">format</a>.
   *
   * @param pbcBytes built contract bytes in .pbc format.
   * @return the .pbc file split into it's sections
   * @throws RuntimeException if the given bytes don't contain the PBSC header string, an ABI
   *     section or a WASM section or if the sectioned part of the bytes is ill-formed.
   */
  public static PbcFile fromBytes(byte[] pbcBytes) {
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(pbcBytes);
    byte[] inputHeaderBytes = inputStream.readBytes(4);
    if (!Arrays.equals(inputHeaderBytes, PBC_HEADER_BYTES)) {
      throw new RuntimeException("Given bytes do not contain PBC header!");
    }

    Sections sections;
    try {
      sections = Sections.fromBytes(inputStream, IDENTIFIERS);
    } catch (RuntimeException e) {
      throw new RuntimeException(
          "Exception encountered while parsing sectioned bytes:\n" + e.getMessage(), e);
    }

    return fromSections(sections);
  }

  private static PbcFile fromSections(Sections sections) {
    if (!sections.hasSection(ABI_IDENTIFIER_BYTE)) {
      throw new RuntimeException("Given bytes contain no ABI section!");
    }
    if (!sections.hasSection(WASM_IDENTIFIER_BYTE)) {
      throw new RuntimeException("Given bytes contain no WASM section!");
    }

    byte[] abiBytes = sections.getSection(ABI_IDENTIFIER_BYTE);
    byte[] wasmBytes = sections.getSection(WASM_IDENTIFIER_BYTE);
    byte[] zkbcBytes = null;
    boolean isZk = false;
    if (sections.hasSection(ZK_IDENTIFIER_BYTE)) {
      zkbcBytes = sections.getSection(ZK_IDENTIFIER_BYTE);
      isZk = true;
    }

    return new PbcFile(abiBytes, wasmBytes, zkbcBytes, isZk);
  }

  /**
   * Build ZKWA formatted bytes, following the definition of the ZKWA format <a href=
   * "https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#zkwa-format">here</a>.
   * Both a WASM and ZKBC section must be present, to create ZKWA bytes.
   *
   * <p>ZKWA is the format used to deploy ZK-contracts to the Partisia Blockchain.
   *
   * @return the ZKWA formatted bytes.
   * @throws RuntimeException if no ZKBC section is present.
   */
  public byte[] getZkwaBytes() {
    if (!isZk) {
      throw new RuntimeException(
          "Could not construct ZKWA file, the ZKBC section was missing in the given bytes.");
    }
    return SafeDataOutputStream.serialize(
        safeDataOutputStream -> {
          safeDataOutputStream.writeByte(WASM_IDENTIFIER_BYTE);
          safeDataOutputStream.writeInt(wasmBytes.length);
          safeDataOutputStream.write(wasmBytes);
          safeDataOutputStream.writeByte(ZK_IDENTIFIER_BYTE);
          safeDataOutputStream.writeInt(zkbcBytes.length);
          safeDataOutputStream.write(zkbcBytes);
        });
  }

  /**
   * Returns the content bytes of the ABI section.
   *
   * @return the ABI content bytes.
   */
  public byte[] getAbiBytes() {
    return abiBytes;
  }

  /**
   * Returns the content bytes of the WASM section.
   *
   * @return the WASM content bytes.
   */
  public byte[] getWasmBytes() {
    return wasmBytes;
  }

  /**
   * Returns the content bytes of the ZKBC section.
   *
   * @return the ZKBC content bytes.
   */
  public byte[] getZkbcBytes() {
    if (!isZk) {
      throw new RuntimeException("Cannot get ZKBC bytes from a public contract");
    }
    return zkbcBytes;
  }
}
