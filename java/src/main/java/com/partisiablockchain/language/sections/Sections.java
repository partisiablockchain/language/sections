package com.partisiablockchain.language.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Sections of a sectioned file. A section follows the following <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#sections">format</a>.
 *
 * <p>A section consists of an identifier byte, a 32-bit unsigned integer indicating the length of
 * the section's content and then the section's content bytes.
 *
 * <p>Can be used to build sectioned files, eg. a <a
 * href="https://partisiablockchain.gitlab.io/documentation/smart-contracts/smart-contract-binary-formats.html#partisia-blockchain-file-format">.pbc
 * file</a>.
 */
public final class Sections {
  /** Map from section identifiers to content bytes. */
  private final HashMap<Byte, byte[]> sections;

  /** Ordered set of allowed identifiers for the given sections. */
  public final List<Byte> allowedIdentifiers;

  /**
   * Creates an empty collection of sections.
   *
   * @param allowedIdentifiers byte array containing the allowed identifiers for the given sections.
   */
  public Sections(List<Byte> allowedIdentifiers) {
    this.sections = new HashMap<>();
    this.allowedIdentifiers = new ArrayList<>(allowedIdentifiers);
  }

  /**
   * Returns the bytes of the section corresponding to the given identifier.
   *
   * @param identifier identifier of the section to be returned
   * @return the section content bytes
   */
  public byte[] getSection(byte identifier) {
    return sections.get(identifier);
  }

  /**
   * Serializes the added sections into a byte array. The sections are ordered by their identifiers
   * from lowest to highest.
   *
   * @return the serialized sections.
   */
  public byte[] getSerializedBytes() {
    return SafeDataOutputStream.serialize(
        safeDataOutputStream -> {
          for (byte identifier : allowedIdentifiers) {
            if (sections.containsKey(identifier)) {
              safeDataOutputStream.writeByte(identifier);
              safeDataOutputStream.writeInt(sections.get(identifier).length);
              safeDataOutputStream.write(sections.get(identifier));
            }
          }
        });
  }

  /**
   * Adds a section of bytes with a given header byte to the collection of sections. If a section
   * with the provided identifier has already been added then the content of that section is
   * overwritten.
   *
   * @param identifier identifier byte indicating the type of the section to be added.
   * @param sectionBytes the binary data of the section to be added.
   * @throws RuntimeException if the given identifier is not in the set of allowed identifiers.
   */
  public void addSection(byte identifier, byte[] sectionBytes) {
    if (!allowedIdentifiers.contains(identifier)) {
      throw new RuntimeException(
          String.format("Section identifier %d is not allowed.", identifier));
    }
    sections.put(identifier, sectionBytes);
  }

  /**
   * Check if the sections contains a section with the given identifier.
   *
   * @param identifier identifier byte indicating the type of the section.
   * @return whether a section with the given identifier is contained.
   */
  public boolean hasSection(byte identifier) {
    return sections.containsKey(identifier);
  }

  /**
   * Creates Sections by parsing sectioned binary data. Sections must come in ascending order by id.
   *
   * @param inputStream Stream of bytes to parse.
   * @param allowedIdentifiers List of allowed section identifiers
   * @return the parsed sections.
   * @exception RuntimeException If the input bytes are encoded incorrectly.
   */
  public static Sections fromBytes(SafeDataInputStream inputStream, List<Byte> allowedIdentifiers) {
    Sections sections = new Sections(allowedIdentifiers);
    byte previousSectionId = 0;
    Byte sectionId = readNextSectionId(inputStream);

    while (sectionId != null) {

      // max id reached
      if (Byte.toUnsignedInt(previousSectionId) == 255) {
        throw new RuntimeException(
            String.format(
                "Invalid section %d: Sections past a section with the id 255 are not allowed.",
                sectionId));
      }

      // Duplicate id
      if (sections.hasSection(sectionId)) {
        throw new RuntimeException(
            String.format("Invalid section %d: Duplicated section id", sectionId));
      }

      // Check that section is correctly ordered by its id
      if (Byte.toUnsignedInt(sectionId) < Byte.toUnsignedInt(previousSectionId)) {
        // Incorrectly ordered
        throw new RuntimeException(
            String.format(
                "Invalid section %d: incorrectly ordered. Expected section"
                    + " with id of at least %d",
                sectionId, Byte.toUnsignedInt(previousSectionId) + 1));
      }

      // Add content bytes to Sections
      byte[] sectionContent = readSectionContent(inputStream);
      sections.addSection(sectionId, sectionContent);

      // Bookkeeping
      previousSectionId = sectionId;
      sectionId = readNextSectionId(inputStream);
    }
    return sections;
  }

  private static Byte readNextSectionId(SafeDataInputStream stream) {
    // Try to read section id
    try {
      return stream.readSignedByte();
    } catch (RuntimeException e) {
      // Return null to indicate that stream is empty
      return null;
    }
  }

  private static byte[] readSectionContent(SafeDataInputStream stream) {
    // Read length of the section's content
    final int sectionLen = stream.readInt();

    // Try to read and return the indicated number of bytes
    return stream.readBytes(sectionLen);
  }
}
