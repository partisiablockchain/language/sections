package com.partisiablockchain.language.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.stream.SafeDataInputStream;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** {@link Sections} testing. */
public final class SectionsTest {

  /* Parse bytes into sections */

  /**
   * It's possible to parse well-formed, sectioned bytes into Sections provided a list of allowed
   * identifiers.
   */
  @Test
  public void parseSectionedBytes() {
    final byte[] data =
        Hex.decode(
            "01 00 00 00 05" // Section 1: 5 bytes
                + "50 51 52 53 54" // Section 1 data
                + "02 00 00 00 00" // Section 2: 0 bytes
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = Sections.fromBytes(inputStream, allowedIdentifiers);
    Assertions.assertTrue(sections.hasSection((byte) 0x01));
    Assertions.assertTrue(sections.hasSection((byte) 0x02));
    Assertions.assertArrayEquals(sections.getSerializedBytes(), data);
  }

  /** Sections must be ordered by their identifiers, lowest to highest. */
  @Test
  public void parseIncorrectlyOrderedSections() {
    final byte[] data =
        Hex.decode(
            "02 00 00 00 00" // Section 2: 0 bytes
                + "01 00 00 00 05" // Section 1: 5 bytes
                + "50 51 52 53 54" // Section 1 data
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    assertThatThrownBy(() -> Sections.fromBytes(inputStream, allowedIdentifiers))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining(
            "Invalid section 1: incorrectly ordered. Expected section with id of at" + " least 3");
  }

  /** No sections are allowed after a section with max id 255. Ids are unsigned bytes. */
  @Test
  public void parseIncorrectlyOrderedSectionsMaxId() {
    final byte[] data =
        Hex.decode(
            "FF 00 00 00 05" // Section 255: 5 bytes
                + "50 51 52 53 54" // Section 255 data
                + "02 00 00 00 00" // Section 2: 0 bytes
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0xFF, (byte) 0x02);
    assertThatThrownBy(
            () -> {
              Sections.fromBytes(inputStream, allowedIdentifiers);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining(
            "Invalid section 2: Sections past a section with the id 255 are not allowed.");
  }

  /** Indicating the length of a section to be negative is not allowed. */
  @Test
  public void parseSectionNegativeLength() {
    final byte[] data =
        Hex.decode(
            "01 FF FF FF FF" // Section 1: -1 bytes
                + "50 51 52 53 54" // Section 1 data
                + "02 00 00 00 00" // Section 2: 0 bytes
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    assertThatThrownBy(
            () -> {
              Sections.fromBytes(inputStream, allowedIdentifiers);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Unable to read bytes");
  }

  /**
   * A section may not be indicated to be longer than the remainder of the proceeding bytes to be
   * parsed.
   */
  @Test
  public void parseSectionLengthTooLong() {
    final byte[] data =
        Hex.decode(
            "01 0F 00 00 00" // Section 1: 251658240 bytes indicated
                + "50 51 52 53 54" // Section 1 data
                + "02 00 00 00 00" // Section 2: 0 bytes
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    assertThatThrownBy(
            () -> {
              Sections.fromBytes(inputStream, allowedIdentifiers);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Unable to read requested number of bytes");
  }

  /** Sectioned bytes may not contain duplicate identifiers. */
  @Test
  public void parseSectionsDuplicateIdentifier() {
    final byte[] data =
        Hex.decode(
            "02 00 00 00 00" // Section 2: 0 bytes
                + "02 00 00 00 05" // Section 2: 5 bytes
                + "50 51 52 53 54" // Section 2 data
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    assertThatThrownBy(() -> Sections.fromBytes(inputStream, allowedIdentifiers))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid section 2: Duplicated section id");
  }

  /** Parsed Sections may not contain identifiers outside the list of allowed identifiers. */
  @Test
  public void parseSectionsNotAllowedIdentifier() {
    final byte[] data =
        Hex.decode(
            "02 00 00 00 00" // Section 2: 0 bytes
                + "03 00 00 00 05" // Section 3: 5 bytes
                + "50 51 52 53 54" // Section 3 data
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    assertThatThrownBy(() -> Sections.fromBytes(inputStream, allowedIdentifiers))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Section identifier 3 is not allowed.");
  }

  /* Add sections */

  /** It's possible to add sections individually given an identifier byte and content bytes. */
  @Test
  void addSection() {
    final byte[] sectionContent = Hex.decode("50 51 52 53 54");
    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x1, sectionContent);

    assertThat(sections.hasSection((byte) 0x01)).isTrue();
    assertThat(sections.getSection((byte) 0x01).length).isEqualTo(sectionContent.length);
    Assertions.assertArrayEquals(sections.getSection((byte) 0x01), sectionContent);
  }

  /**
   * Adding a section with an identifier that has already been added overwrites the content of that
   * section.
   */
  @Test
  void overwriteExistingSection() {
    final byte[] sectionContent = Hex.decode("50 51 52 53 54");
    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x1, sectionContent);
    Assertions.assertArrayEquals(sections.getSection((byte) 0x1), sectionContent);
    final byte[] sectionContentAltered = Hex.decode("50 51 52 53 55");
    sections.addSection((byte) 0x1, sectionContentAltered);
    Assertions.assertArrayEquals(sections.getSection((byte) 0x1), sectionContentAltered);
  }

  /** Section ids larger than 127 are treated as unsigned integers. */
  @Test
  void addSectionWithIdLargerThan127() {
    final byte[] data =
        Hex.decode(
            "00 00 00 00 00" // Section 0: 0 bytes
                + "80 00 00 00 00" // Section 128: 0 bytes (should be treated as unsigned integer)
                + "FF 00 00 00 00" // Section 255: 0 bytes (should be treated as unsigned integer)
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers = List.of((byte) 0x00, (byte) 0x80, (byte) 0xFF);
    Sections sections = Sections.fromBytes(inputStream, allowedIdentifiers);
    assertThat(sections.hasSection((byte) 0x00)).isTrue();
    assertThat(sections.hasSection((byte) 0x80)).isTrue();
    assertThat(sections.hasSection((byte) 0xFF)).isTrue();
  }

  /**
   * An added section must not have an identifier which is not given in the list of allowed
   * identifiers.
   */
  @Test
  public void addSectionWithIllegalIdentifier() {
    final byte[] data = Hex.decode("01 02 03 04 05");
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = new Sections(allowedIdentifiers);
    assertThatThrownBy(() -> sections.addSection((byte) 0x03, data))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Section identifier 3 is not allowed.");
  }

  /* Storing sections and querying stored sections */

  /** It is possible to retrieve the content of an added section by its identifier. */
  @Test
  public void getSection() {
    byte[] section1Content = Hex.decode("50 51 52 53 54");
    byte[] section2Content = Hex.decode("55 56 57 58 59");

    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x01, section1Content);
    sections.addSection((byte) 0x02, section2Content);

    assertThat(sections.getSection((byte) 0x01)).isEqualTo(section1Content);
    assertThat(sections.getSection((byte) 0x02)).isEqualTo(section2Content);
  }

  /** Querying the identifier of a section that has not been added returns null. */
  @Test
  public void getSectionNotPresent() {
    byte[] section1Content = Hex.decode("50 51 52 53 54");

    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x01, section1Content);

    assertThat(sections.getSection((byte) 0x02)).isNull();
  }

  /* Serializing stored sections */

  /**
   * It's possible to serialize added sections into well-formed, sectioned bytes.
   *
   * <p>Each section contains an identifier, a 32-bit, unsigned, big-endian integer indicating the
   * length of the section and finally the content bytes of the section.
   */
  @Test
  void serializeAddedSections() {
    byte[] section1 = Hex.decode("50 51 52 53 54");
    byte[] section2 = Hex.decode("65 66 67 68 69");

    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x01, section1);
    sections.addSection((byte) 0x02, section2);

    // Perform assertions
    SafeDataInputStream sectionsBytesStream =
        SafeDataInputStream.createFromBytes(sections.getSerializedBytes());
    Assertions.assertEquals(sectionsBytesStream.readSignedByte(), (byte) 0x01);
    Assertions.assertEquals(sectionsBytesStream.readInt(), section1.length);
    Assertions.assertArrayEquals(sectionsBytesStream.readBytes(section1.length), section1);
    Assertions.assertEquals(sectionsBytesStream.readSignedByte(), (byte) 0x02);
    Assertions.assertEquals(sectionsBytesStream.readInt(), section2.length);
    Assertions.assertArrayEquals(sectionsBytesStream.readBytes(section2.length), section2);
  }

  /**
   * The serialized sections are ordered by identifier from smallest to largest even if they are
   * added in opposite order.
   */
  @Test
  void addSectionsOppositeOrder() {
    byte[] section1Content = Hex.decode("50 51 52 53 54");
    byte[] section2Content = Hex.decode("55 56 57 58 59");

    // Create sections
    final List<Byte> allowedIdentifiers = List.of((byte) 0x01, (byte) 0x02);
    Sections sections = new Sections(allowedIdentifiers);
    sections.addSection((byte) 0x02, section2Content);
    sections.addSection((byte) 0x01, section1Content);

    // Perform assertions
    SafeDataInputStream sectionsBytesStream =
        SafeDataInputStream.createFromBytes(sections.getSerializedBytes());
    Assertions.assertEquals(sectionsBytesStream.readSignedByte(), (byte) 0x01);
    Assertions.assertEquals(sectionsBytesStream.readInt(), section1Content.length);
    Assertions.assertArrayEquals(
        sectionsBytesStream.readBytes(section1Content.length), section1Content);
    Assertions.assertEquals(sectionsBytesStream.readSignedByte(), (byte) 0x02);
    Assertions.assertEquals(sectionsBytesStream.readInt(), section2Content.length);
    Assertions.assertArrayEquals(
        sectionsBytesStream.readBytes(section2Content.length), section2Content);
  }

  /**
   * Only the sections present in the parsed input bytes are present when the parsed sections are
   * serialized, despite the list of allowed identifiers containing more.
   */
  @Test
  public void getSerializedBytesManyAllowedIdentifiers() {
    final byte[] data =
        Hex.decode(
            "02 00 00 00 00" // Section 2: 0 bytes
                + "03 00 00 00 05" // Section 3: 5 bytes
                + "50 51 52 53 54" // Section 3 data
            );
    SafeDataInputStream inputStream = SafeDataInputStream.createFromBytes(data);
    final List<Byte> allowedIdentifiers =
        List.of((byte) 0x01, (byte) 0x02, (byte) 0x03, (byte) 0x04, (byte) 0x05);
    Sections sections = Sections.fromBytes(inputStream, allowedIdentifiers);
    assertThat(sections.getSerializedBytes()).isEqualTo(data);
  }
}
