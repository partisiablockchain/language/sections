package com.partisiablockchain.language.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.secata.tools.coverage.ExceptionConverter;
import java.nio.file.Files;
import java.nio.file.Path;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** {@link PbcFile} testing. */
public final class PbcFileTest {

  /* Parsing .pbc files from bytes */

  /** PbcFile can be created from bytes. */
  @Test
  public void createPbcFileFromBytes() {
    Path zkVotingSimplePbcPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.pbc");
    byte[] zkVotingPbcBytes = readBytesFromFilePath(zkVotingSimplePbcPath);
    PbcFile file = PbcFile.fromBytes(zkVotingPbcBytes);

    Path zkVotingSimpleAbiPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.abi");
    byte[] expectedAbi = readBytesFromFilePath(zkVotingSimpleAbiPath);
    Path zkVotingSimpleZkwaPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.zkwa");
    byte[] expectedZkwa = readBytesFromFilePath(zkVotingSimpleZkwaPath);

    assertThat(file.getAbiBytes()).isEqualTo(expectedAbi);
    assertThat(file.getZkwaBytes()).isEqualTo(expectedZkwa);
  }

  /** A byte representation of a PBC file, must contain the PBC header, "PBSC". */
  @Test
  public void createFromBytesWithoutPbcHeader() {
    byte[] pbcBytes =
        Hex.decode(
            "01 00 00 00 00" // Section 1: 0 bytes
                + "02 00 00 00 05" // Section 2: 5 bytes
                + "50 51 52 53 54" // Section 2 data
            );

    assertThatThrownBy(() -> PbcFile.fromBytes(pbcBytes))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Given bytes do not contain PBC header!");
  }

  /** The byte representation of a PBC file must contain an ABI section. */
  @Test
  public void createFromBytesWithoutAbiSection() {
    byte[] pbcBytesNoAbi =
        Hex.decode(
            "50 42 53 43" // PBSC header bytes
                + "02 00 00 00 05" // Section 2: 5 bytes
                + "50 51 52 53 54" // Section 2 data
            );

    assertThatThrownBy(() -> PbcFile.fromBytes(pbcBytesNoAbi))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Given bytes contain no ABI section!");
  }

  /** The byte representation of a PBC file must contain a WASM section. */
  @Test
  public void createFromBytesWithoutWasmSection() {
    byte[] pbcBytes =
        Hex.decode(
            "50 42 53 43" // PBSC header bytes
                + "01 00 00 00 05" // Section 1: 5 bytes
                + "50 51 52 53 54" // Section 1 data
            );

    // Make assertions
    assertThatThrownBy(() -> PbcFile.fromBytes(pbcBytes))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Given bytes contain no WASM section!");
  }

  /** The sections of a PBC file must be correctly ordered. */
  @Test
  public void createFromBytesWithIncorrectlyOrderedSections() {
    final byte[] data =
        Hex.decode(
            "50 42 53 43" // PBSC header bytes
                + "02 00 00 00 00" // Section 2: 0 bytes
                + "01 00 00 00 05" // Section 1: 5 bytes
                + "50 51 52 53 54" // Section 1 data
            );

    assertThatThrownBy(
            () -> {
              PbcFile.fromBytes(data);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid section 1: incorrectly ordered.");
  }

  /** A PBC file must not contain duplicate sections. */
  @Test
  public void createFromBytesWithDuplicateSections() {
    final byte[] data =
        Hex.decode(
            "50 42 53 43" // PBSC header bytes
                + "02 00 00 00 00" // Section 2: 0 bytes
                + "02 00 00 00 05" // Section 2: 5 bytes
                + "50 51 52 53 54" // Section 1 data
            );

    assertThatThrownBy(
            () -> {
              PbcFile.fromBytes(data);
            })
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Invalid section 2: Duplicated section id");
  }

  /* Querying sections of a parsed .pbc file */

  /** WASM bytes can be extracted from a .pbc file. */
  @Test
  public void getWasmBytes() {
    // Read references
    Path zkVotingSimplePath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.pbc");
    byte[] zkVotingSimpleBytes = readBytesFromFilePath(zkVotingSimplePath);

    Path zkVotingSimpleWasmPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.wasm");
    byte[] zkVotingSimpleWasmBytes = readBytesFromFilePath(zkVotingSimpleWasmPath);

    PbcFile zkVotingSimplePbcFile = PbcFile.fromBytes(zkVotingSimpleBytes);

    Assertions.assertArrayEquals(zkVotingSimplePbcFile.getWasmBytes(), zkVotingSimpleWasmBytes);
  }

  /** ZKBC bytes can be extracted from the .pbc file of a zk-contract. */
  @Test
  public void getZkbcBytes() {
    // Read references
    Path zkVotingSimplePath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.pbc");
    byte[] zkVotingSimpleBytes = readBytesFromFilePath(zkVotingSimplePath);

    Path zkVotingSimpleZkbcPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.zkbc");
    byte[] zkVotingSimpleZkbcBytes = readBytesFromFilePath(zkVotingSimpleZkbcPath);

    PbcFile zkVotingSimplePbcFile = PbcFile.fromBytes(zkVotingSimpleBytes);

    Assertions.assertArrayEquals(zkVotingSimplePbcFile.getZkbcBytes(), zkVotingSimpleZkbcBytes);
  }

  /**
   * ZKWA bytes can be constructed from the WASM section and ZKBC section of a .pbc file of a
   * zk-contract.
   */
  @Test
  public void getZkwaBytes() {
    // Read references
    Path zkVotingSimplePath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.pbc");
    byte[] zkVotingSimpleBytes = readBytesFromFilePath(zkVotingSimplePath);

    Path zkVotingSimpleZkwaPath =
        Path.of("../resources/references/zk_voting_simple/zk_voting_simple.zkwa");
    byte[] zkVotingSimpleZkwaBytes = readBytesFromFilePath(zkVotingSimpleZkwaPath);

    PbcFile zkVotingSimplePbcFile = PbcFile.fromBytes(zkVotingSimpleBytes);

    Assertions.assertArrayEquals(zkVotingSimplePbcFile.getZkwaBytes(), zkVotingSimpleZkwaBytes);
  }

  /** ZKBC bytes cannot be extracted from the .pbc file of a public contract. */
  @Test
  public void getZkbcBytesPublicContract() {
    // Read references
    Path multiVotingSimplePath =
        Path.of("../resources/references/multi_voting_contract/multi_voting_contract.pbc");
    byte[] multiVotingSimpleBytes = readBytesFromFilePath(multiVotingSimplePath);

    PbcFile multiVotingSimplePbcFile = PbcFile.fromBytes(multiVotingSimpleBytes);

    assertThatThrownBy(multiVotingSimplePbcFile::getZkbcBytes)
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Cannot get ZKBC bytes from a public contract");
  }

  /** ZKWA bytes cannot be constructed from a public contract. */
  @Test
  public void getZkwaBytesPublicContract() {
    // Create PbcFile
    Path multiVotingPbcPath =
        Path.of("../resources/references/multi_voting_contract/multi_voting_contract.pbc");
    byte[] multiVotingPbcBytes = readBytesFromFilePath(multiVotingPbcPath);
    PbcFile multiVotingPbcFile =
        ExceptionConverter.call(() -> PbcFile.fromBytes(multiVotingPbcBytes));

    // Make assertion
    assertThatThrownBy(multiVotingPbcFile::getZkwaBytes)
        .hasMessageContaining(
            "Could not construct ZKWA file, the ZKBC section was missing in the given bytes.");
  }

  private byte[] readBytesFromFilePath(Path path) {
    return ExceptionConverter.call(() -> Files.readAllBytes(path));
  }
}
